import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { GuidedTourModule, GuidedTourService } from 'ngx-guided-tour';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, GuidedTourModule],
  providers: [GuidedTourService],
  bootstrap: [AppComponent],
})
export class AppModule {}
