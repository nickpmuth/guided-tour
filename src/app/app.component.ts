import { Component, ElementRef, ViewChild, Renderer2 } from '@angular/core';
import { GuidedTour, GuidedTourService, Orientation } from 'ngx-guided-tour';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'guided-tour';
  @ViewChild('newComponenetRef') newComponenetRef: ElementRef;
  @ViewChild('commandsRef') commandsRef: ElementRef;

  public dashboardTour: GuidedTour = {
    tourId: 'dashboard-tour',
    useOrb: false,
    preventBackdropFromAdvancing: true,
    steps: [
      {
        title: 'Welcome',
        content:
          'Here you will find Angular resources and learn about different CLI commands.',
      },
      {
        selector: 'h2',
        useHighlightPadding: true,
        content: 'Here you will find links to help you get started.',
        orientation: Orientation.Right,
      },
      {
        selector: '.new-component',
        skipStep: false,
        content:
          'Click the buttons below to generate the different CLI commands.',
        action: () => {
          this.renderer.addClass(
            this.newComponenetRef.nativeElement,
            'active-feature'
          );
          this.newComponenetRef.nativeElement.addEventListener(
            'click',
            (e: Event) => {
              this.guidedTourService.nextStep();
            }
          );
        },
        closeAction: () => {
          this.renderer.removeClass(
            this.newComponenetRef.nativeElement,
            'active-feature'
          );
        },
        orientation: Orientation.Top,
      },
      {
        selector: '.terminal',
        content: 'Output is shown here.',
        orientation: Orientation.Bottom,
      },
      {
        selector: '.commands',
        content: 'Try clicking some more action buttons.',
        orientation: Orientation.Top,
        action: () => {
          this.renderer.addClass(
            this.commandsRef.nativeElement,
            'active-feature'
          );
        },
      },
    ],
  };

  constructor(
    private guidedTourService: GuidedTourService,
    private renderer: Renderer2
  ) {
    setTimeout(() => {
      this.guidedTourService.startTour(this.dashboardTour);
    }, 500);
  }

  public restartTour(): void {
    this.guidedTourService.startTour(this.dashboardTour);
  }

  private onNewComponentClick(): void {}
}
